#!/bin/bash
# Setup script to will install cfw & white/blacklists in /opt/iptables.

function install_fw ()
{
	FW_DIR=/opt/iptables

	if [ -d $FW_DIR ]; then 
 		echo "Directory: $FW_DIR exists"
	else
		echo "Dirctory: $FW_DIR not found. Creating.."
		if [ $UID -ne 0 ]; then
		    sudo mkdir $FW_DIR
		    sudo cp cfw.sh blacklist.txt whitelist.txt $FW_DIR

		else
		    mkdir $FW_DIR
		    sudo cp cfw.sh blacklist.txt whitelist.txt $FW_DIR
		fi
	fi
}

install_fw


