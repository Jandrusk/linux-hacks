# cfw - Custom Firewall Script
#!/bin/bash
#

# A simple firewall init script

# Make sure script is running as root. 
if [ $UID -ne 0 ]; then
    IPT="sudo iptables"
else
    IPT="iptables"
fi

WHITELIST=whitelist.txt
BLACKLIST=blacklist.txt
ALLOWED_PORTS="22 443" # Change these based on what you want to accept.

# Flush existing rules
$IPT -F

# First run through $WHITELIST, accepting all traffic from the hosts and networks contained therein.
for x in `grep -v ^# $WHITELIST | awk '{print $1}'`; do
    echo "Permitting $x.."
    $IPT -A INPUT -t filter -s $x -j ACCEPT
done

# Now run through $BLACKLIST, dropping all traffic from the hosts and networks contained therein.
for x in `grep -v ^# $BLACKLIST | awk '{print $1}'`; do
    echo "Blocking $x..."
    $IPT -A INPUT -t filter -s $x -j DROP
done

#
# Next the permitted ports: What will we accept from hosts not appearing on the blacklist?
#

for port in $ALLOWED_PORTS; do
    echo "Accepting port $port..."
    $IPT -A INPUT =t filter -p tcp --dport $port -j ACCEPT
done

